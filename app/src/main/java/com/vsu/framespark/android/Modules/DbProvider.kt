package com.vsu.framespark.android.Modules

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides

@Module
class DbProvider {

    @Provides
    fun provideDB() : FirebaseFirestore{
        return Firebase.firestore
    }
}
package com.vsu.framespark.android

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.vsu.framespark.android.Models.User

class AdminActivity : AppCompatActivity() {
    var db: FirebaseFirestore? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        db = Firebase.firestore

        var mSettings: SharedPreferences? = getSharedPreferences("user", Context.MODE_PRIVATE)
        val id = mSettings!!.getString("id", "")
        readSetData(id!!)

    }

    fun readSetData(id: String){
        var user: Task<QuerySnapshot> = db!!.collection("users")
            .whereEqualTo("id",id)
            .get()
            .addOnSuccessListener { documentReference ->
                val login : TextView = findViewById(R.id.textView8)
                login.text = documentReference.documents.get(0).get("fio").toString()
            }
        System.out.println("all")
    }
}
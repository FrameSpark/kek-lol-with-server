package com.vsu.framespark.android

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.vsu.framespark.android.Interfaces.AppComponent
import com.vsu.framespark.android.Interfaces.DaggerAppComponent
import com.vsu.framespark.android.Models.Auth
import com.vsu.framespark.android.Models.User
import com.vsu.framespark.android.Services.AuthService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    var mSettings: SharedPreferences? = null
    @Inject
    lateinit var db: FirebaseFirestore

    lateinit var appComponent: AppComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mSettings = getSharedPreferences("user", Context.MODE_PRIVATE)
        appComponent = DaggerAppComponent.create()
        appComponent.injectMainActivity(this)
        //db = Firebase.firestore
        System.out.println("test")
    }

     fun toGrid(view: View){
        val emailView : TextView = findViewById(R.id.editTextTextEmailAddress)
        val passwordView : TextView = findViewById(R.id.editTextTextPassword)

        val auth : Auth = Auth()
        auth.mail = emailView.text.toString()
        auth.password = passwordView.text.toString()

         if(!auth.mail.toString().equals("user") && !auth.mail.toString().equals("admin")) {
             val duration = Toast.LENGTH_SHORT
             val toast = Toast.makeText(applicationContext, "Неверные креды", duration)
             toast.show()
             return;
         }

        val callListPost = AuthService.inctance?.JSONApi?.auth(auth)

         var user:User
        val callback = object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                try {
                    user = response.body()!!
                }
                catch (e: Exception) {
                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(applicationContext, "Неверные креды", duration)
                    toast.show()
                    return;
                }

                val editor = mSettings!!.edit()
                editor.putString("id", user.id)
                editor.apply()

                db!!.collection("users")
                    .add(user)
                    .addOnSuccessListener { documentReference ->
                        System.out.println("success")
                    }
                    .addOnFailureListener { e ->
                        System.out.println("error")
                    }


                if(user.id == "1"){
                    val intent = Intent(this@MainActivity, AdminActivity::class.java)
                    startActivity(intent)
                }
                else {
                    val intent = Intent(this@MainActivity, GridActivity::class.java)
                    startActivity(intent)
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(applicationContext,  "Ошибка загрузки или неверный пароль", duration)
                toast.show()
            }
        }
        //callListPost?.enqueue()
        callListPost?.enqueue(callback)
    }
}
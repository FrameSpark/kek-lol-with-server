package com.vsu.framespark.android.Interfaces

import com.vsu.framespark.android.MainActivity
import com.vsu.framespark.android.Modules.DbProvider
import dagger.Component

@Component(modules = [DbProvider::class])
interface AppComponent {
    fun injectMainActivity(mainActivity: MainActivity)
}
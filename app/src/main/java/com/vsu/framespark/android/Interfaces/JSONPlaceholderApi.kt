package com.vsu.framespark.android.Interfaces

import com.vsu.framespark.android.Models.People
import com.vsu.framespark.android.Models.User
import com.vsu.framespark.android.Models.Auth
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface JSONPlaceholderApi {
    @GET("/data")
    fun getAdvice(): Call<List<People>>

    @POST("/student/auth")
    fun auth(@Body auth : Auth): Call<User>
}